package com.chao.phase1.model3;

/**
 * 编程获取两个指定字符串中的最大相同子串。
 * <p>
 * 如： s1="asdafghjka", s2="aaasdfg" 他们的最大子串为"asd"
 * <p>
 * 提示： 将短的那个串进行长度依次递减的子串与较长的串比较。
 *
 * @author chaobo
 * @date 2021/3/13 11:15
 */
public class Question3_2 {

    public static void main(String[] args) {
        System.out.println(text("asdafghjka", "aaabsdfg"));
    }


    public static String text(String s1, String s2) {
        String max = "";
        String min = "";
        max = (s1.length() > s2.length()) ? s1 : s2;
        min = (max.equals(s1)) ? s2 : s1;


        for (int x = 0; x < min.length(); x++) {
            for (int y = 0, z = min.length() - x; z != min.length() + 1; y++, z++) {
                String temp = min.substring(y, z);
                if (max.contains(temp)) {
                    return temp;
                }
            }
        }
        return null;

    }

}
