package com.chao.phase1.model3;

import java.util.Scanner;

/**
 * 编程统计字符串"ABCD123!@#$%ab"中大写字母、小写字母、数字、其它字符的个数并打 印出来。
 *
 * @author chaobo
 * @date 2021/3/13 11:15
 */
public class Question3_1 {

    public static void main(String[] args) {
        //1.提示用户输入一串字符串
        System.out.println("请输入一串字符串");
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        //5.使用变量储存字符数量
        //大写字符数量
        int big = 0;
        //小写字符数量
        int small = 0;
        //数字字符数量
        int number = 0;
        // 其他字符数量
        int other = 0;

        //2.将用户输入的String字符串转换成char数组
        char[] cArr = str.toCharArray();

        //3.使用for循环遍历数组内容
        for (int i = 0; i < cArr.length; i++) {

            //4.使用if else分支语句判断数组内字符的类别
            if ('A' <= cArr[i] && cArr[i] <= 'Z') {
                big++;
            } else if ('a' <= cArr[i] && cArr[i] <= 'z') {
                small++;
            } else if ('0' <= cArr[i] && cArr[i] <= '9') {
                number++;
            } else {
                other++;
            }
        }

        //6.打印输出结果
        System.out.println("大写字母字符的数量是" + big);
        System.out.println("小写字母字符的数量是" + small);
        System.out.println("数字字符的数量是" + number);
        System.out.println("其他字符的数量是" + other);
    }
}
