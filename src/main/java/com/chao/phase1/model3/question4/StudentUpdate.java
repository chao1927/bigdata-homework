package com.chao.phase1.model3.question4;

import java.util.List;
import java.util.Scanner;

/**
 * @author chaobo
 * @date 2021/3/13 11:27
 */
public class StudentUpdate {

    public static List<Student> updateStudent(List<Student> list) {
        System.out.println("请输入要修改学生的id：");
        Scanner sc = new Scanner(System.in);
        int id = sc.nextInt();
        for (int i = 0; i < list.size(); i++) {
            Student o = list.get(i);
            if (o.getId().equals(id)) {
                System.out.println("请输入要修改的age :");
                o.setAge(sc.nextInt());
                System.out.println("请输入要修改的name :");
                o.setName(sc.next());
            } else {
                System.out.println("系统中不存在此学生信息！！！！");
            }
        }
        return list;
    }

}
