package com.chao.phase1.model3.question4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 使用 List 集合实现简易的学生信息管理系统，要求打印字符界面提示用户选择相应的功 能，根据用户输入的选择去实现增加、删除、修改、查找以及遍历所有学生信息的功能。
 * <p>
 * 其中学生的信息有：学号、姓名、年龄。 要求： 尽量将功能拆分为多个.java 文件。
 *
 * @author chaobo
 * @date 2021/3/13 11:15
 */
public class Question3_4 {

    public static void main(String[] args) {
        System.out.println("                  ===========================================");
        System.out.println("                  =   ===========学生信息管理系统=========    = ");
        System.out.println("                  =   =       1 ：添加学生信息           =    = ");
        System.out.println("                  =   =       2 ：删除学生信息           =    = ");
        System.out.println("                  =   =       3 ：修改学生信息           =    = ");
        System.out.println("                  =   =       4 ：查询学生信息           =    = ");
        System.out.println("                  =   =       5 ：退出学生系统           =    = ");
        System.out.println("                  =   ===================================    = ");
        System.out.println("                  ============================================");
        List<Student> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("        请输入：");
            int i = scanner.nextInt();
            switch (i) {
                case 1:
                    StudentAdd.addStudent(list);
                    break;
                case 2:
                    StudentDelete.deleteStudent(list);
                    break;
                case 3:
                    StudentUpdate.updateStudent(list);
                    break;
                case 4:
                    Student_se.find(list);
                    break;
                case 5:
                    return;
                default:
            }
        }
    }

}
