package com.chao.phase1.model3.question4;

import java.util.List;
import java.util.Scanner;

/**
 * @author chaobo
 * @date 2021/3/13 11:28
 */
public class StudentAdd {

    public static List<Student> addStudent(List<Student> list ){
        Scanner sc=new Scanner(System.in);
        Student stu =new Student();
        System.out.print("请输入要添加的id :");
        Integer id = sc.nextInt();
        System.out.print("请输入要添加的name :");
        String name = sc.next();
        System.out.print("请输入要添加的age :");
        Integer age = sc.nextInt();
        stu.setId(id);
        stu.setName(name);
        stu.setAge(age);
        list.add(stu);
        System.out.println(list);
        return list;
    }

}
