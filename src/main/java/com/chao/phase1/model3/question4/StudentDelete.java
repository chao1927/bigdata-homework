package com.chao.phase1.model3.question4;

import java.util.List;
import java.util.Scanner;

/**
 * @author chaobo
 * @date 2021/3/13 11:28
 */
public class StudentDelete {

    public static List<Student> deleteStudent(List<Student> list) {
        System.out.println("请输入要删除学生的id：");
        Scanner sc = new Scanner(System.in);
        int id = sc.nextInt();
        for (Student o : list) {
            if (o.getId().equals(id)) {
                list.remove(o);
                break;
            } else {
                System.out.println("集合中不存在此元素！！！！");
                break;
            }
        }
        return list;
    }

}
