package com.chao.phase1.model3.question4;

import java.util.List;
import java.util.Scanner;

/**
 * @author chaobo
 * @date 2021/3/13 11:28
 */
public class Student_se {

    public static void find(List<Student> list) {
        System.out.println("                  ==================================================");
        System.out.println("                  =   ===========学生信息管理系统===============    = ");
        System.out.println("                  =   =       1 ：查询所有学生信息             =    = ");
        System.out.println("                  =   =       2 ：根据id查询学生信息           =    = ");
        System.out.println("                  =   =       3 ：根据age查询学生信息          =    = ");
        System.out.println("                  =   =       4 ：根据name查询学生信息         =    = ");
        System.out.println("                  =   =       5 ：退出                        =    = ");
        System.out.println("                  =   =========================================   = ");
        System.out.println("                  ==================================================");
        Scanner sc = new Scanner(System.in);
        out:
        while (true) {
            System.out.print("             请输入：");
            int i = sc.nextInt();
            switch (i) {
                case 1:
                    findAll(list);
                    break;
                case 2:
                    findById(list);
                    break;
                case 3:
                    findByAge(list);
                    break;
                case 4:
                    findByName(list);
                    break;
                case 5:
                    break out;
                default:
            }
        }
    }

    public static void findAll(List<Student> list) {
        for (Student student : list) {
            System.out.println("学生的信息： " + student);
        }
    }

    public static void findById(List<Student> list) {
        System.out.print("请输入要查询学生的id：");
        Scanner sc = new Scanner(System.in);
        int id = sc.nextInt();
        for (int i = 0; i < list.size(); i++) {
            Student o = list.get(i);
            if (o == null) {
                break;
            }
            if (o.getId().equals(id)) {
                System.out.println("该学生的信息: 年龄" + o.getAge() + "姓名：" + o.getName());
                break;
            } else {
                System.out.println("系统中不存在此学生信息！！！！");
                break;
            }
        }
    }

    public static void findByAge(List<Student> list) {
        System.out.print("请输入要查询学生的age：");
        Scanner sc = new Scanner(System.in);
        int age = sc.nextInt();
        for (int i = 0; i < list.size(); i++) {
            Student o = list.get(i);
            if (o == null) {
                break;
            }
            if (o.getAge().equals(age)) {
                System.out.println("该学生的信息: 年龄" + o.getAge() + "姓名：" + o.getName());
                break;
            } else {
                System.out.println("系统中不存在此学生信息！！！！");
                break;
            }
        }
    }

    public static void findByName(List<Student> list) {
        System.out.print("请输入要查询学生的name：");
        Scanner sc = new Scanner(System.in);
        String name = sc.next();
        for (int i = 0; i < list.size(); i++) {
            Student o = list.get(i);
            if (o == null) {
                break;
            }
            if (o.getName().equals(name)) {
                System.out.println("该学生的信息: 年龄" + o.getAge() + "姓名：" + o.getName());
                break;
            } else {
                System.out.println("系统中不存在此学生信息！！！！");
                break;
            }
        }
    }
}
