package com.chao.phase1.model3;

import java.util.HashMap;
import java.util.Map;

/**
 * 准备一个 HashMap 集合，统计字符串"123,456,789,123,456"中每个数字字符串出现的次数并打印出来。
 * <p>
 * 如：
 * <p>
 * 123 出现了 2 次
 * <p>
 * 456 出现了 2 次
 * <p>
 * 789 出现了 1 次
 *
 * @author chaobo
 * @date 2021/3/13 11:15
 */
public class Question3_3 {

    public static void main(String[] args) {

        //1.准备一个Map集合
        Map<String, Integer> m1 = new HashMap<>();

        //2.准备一个String类型的对象描述原始字符串
        String str1 = "123,456,123,789,456";

        //3.使用split方法对原始字符串按照字符串中的，拆分字符串，并在集合中查找
        String[] sArr = str1.split(",");
        for (String s : sArr) {

            // 4.若集合中没有该字符串，则将该字符串和1组成一个键值对放入集合中
            if (!m1.containsKey(s)) {
                m1.put(s, 1);
            } else {
                // 5.若集合中有该字符串，则将该字符串对应的value值+1
                m1.put(s, m1.get(s) + 1);
            }
        }
        System.out.println("m1= " + m1);

        //6.获取Map集合中所有的映射关系组成Set集合并遍历
        m1.forEach((k, v) -> System.out.println(k + "出现了" + v + "次！"));
    }
}
