package com.chao.phase1.model3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 使用集合实现斗地主游戏的部分功能，要求如下：
 * <p>
 * （1）首先准备 54 张扑克牌并打乱顺序。
 * <p>
 * （2）由三个玩家交替摸牌，每人 17 张扑克牌，最后三张留作底牌。
 * <p>
 * （3）查看三个玩家手中的扑克牌和底牌。
 * <p>
 * （4）其中玩家手中的扑克牌需要按照大小顺序打印，规则如下：
 * <p>
 * 手中扑克牌从大到小的摆放顺序：大王,小王,2,A,K,Q,J,10,9,8,7,6,5,4,3
 *
 * @author chaobo
 * @date 2021/3/13 11:15
 */
public class Question3_5 {

    public static void main(String[] args) {
        //**1:准备牌操作*/
        //1.1 创建牌盒 将来存储牌面的
        List<String> pokerBox = new ArrayList<>();
        //1.2 创建花色集合
        List<String> colors = new ArrayList<>();
        //1.3 创建数字集合
        ArrayList<String> numbers = new ArrayList<>();
        //1.4 分别给花色 以及 数字集合添加元素
        colors.add("♥");
        colors.add("♦");
        colors.add("♠");
        colors.add("♣");
        for (int i = 2; i <= 10; i++) {
            numbers.add(i + "");
        }
        numbers.add("J");
        numbers.add("Q");
        numbers.add("K");
        numbers.add("A");
        //1.5 创造牌  拼接牌操作
        // 拿出每一个花色  然后跟每一个数字 进行结合  存储到牌盒中
        for (String color : colors) {
            //color每一个花色
            //遍历数字集合
            for (String number : numbers) {
                //存储到牌盒中
                pokerBox.add(color + number);
            }
        }
        //1.6大王小王
        pokerBox.add("小☺");
        pokerBox.add("大☠");
        // System.out.println(pokerBox);
        //洗牌 是不是就是将  牌盒中 牌的索引打乱
        // Collections类  工具类  都是 静态方法
        // shuffer方法*/
        /*
         *static void shuffle (List < ? > list)
         *使用默认随机源对指定列表进行置换。
         */
        //2:洗牌
        Collections.shuffle(pokerBox);
        //3 发牌
        //3.1 创建 三个 玩家集合  创建一个底牌集合
        ArrayList<String> player1 = new ArrayList<String>();
        ArrayList<String> player2 = new ArrayList<String>();
        ArrayList<String> player3 = new ArrayList<String>();
        ArrayList<String> dipai = new ArrayList<String>();
        //遍历 牌盒  必须知道索引
        for (int i = 0; i < pokerBox.size(); i++) {
            //获取 牌面
            String card = pokerBox.get(i);
            //留出三张底牌 存到 底牌集合中
            //存到底牌集合中
            if (i >= 51) {
                dipai.add(card);
            } else {
                //玩家1   %3  ==0
                if (i % 3 == 0) {
                    player1.add(card);
                } else if (i % 3 == 1) {
                    //玩家2
                    player2.add(card);
                } else {
                    //玩家3
                    player3.add(card);
                }
            }
        }
        player1.sort((o1, o2) -> {
            char c1 = o2.charAt(1);
            char c = o1.charAt(1);
            return c1 - c;
        });
        System.out.println("令狐冲：" + player1);
        System.out.println("田伯光：" + player2);
        System.out.println("绿竹翁：" + player3);
        System.out.println("底牌：" + dipai);
    }

}
