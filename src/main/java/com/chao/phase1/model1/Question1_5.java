package com.chao.phase1.model1;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * 5. 编程题
 * 使用双重循环实现五子棋游戏棋盘的绘制， 棋盘界面的具体效果如下：
 *
 * @author chaobo
 * @date 2021/1/7 12:20
 */
public class Question1_5 {

    public static void main(String[] args) {
        int[] arr = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
        for (int i = 0; i < arr.length; i++) {
            if (0 == i) {
                System.out.print(' ');
                System.out.print(' ');
                Arrays.stream(arr).map(value -> ' ' + (char) value + ' ').forEach(System.out::print);
                System.out.println();
            }
            System.out.print((char) arr[i]);
            System.out.print(' ');
            IntStream.range(0, arr.length).mapToObj(j -> " + ").forEach(System.out::print);
            System.out.println();
        }
    }
}
