package com.chao.phase1.model1;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 4. 编程题
 * 自定义数组扩容规则，当已存储元素数量达到总容量的 80%时，扩容 1.5 倍。
 * 例如，总容量是 10，当输入第 8 个元素时，数组进行扩容，容量从 10 变 15。
 *
 * @author chaobo
 * @date 2021/1/7 12:19
 */
public class Question1_4 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        int[] arr = new int[1];
        int count = 0;
        while (flag) {
            System.out.print("请输入数字：");
            int input = sc.nextInt();
            arr[count] = input;

            if (count >= (arr.length - 1) * 0.8) {
                int[] brr = new int[(int) ((arr.length * 1.5 > arr.length * 15 / 10) ? (arr.length * 15 / 10 + 1) : (arr.length * 15 / 10))];
                System.arraycopy(arr, 0, brr, 0, arr.length);
                arr = brr;
            }
            count++;
            System.out.println("数组长度：" + arr.length);
            System.out.println("元素数量：" + count);
            System.out.println("数组内容：" + Arrays.toString(arr));
        }
    }
}

