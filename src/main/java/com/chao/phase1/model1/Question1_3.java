package com.chao.phase1.model1;

import java.util.Arrays;
import java.util.Random;

/**
 * 3. 编程题
 * 实现双色球抽奖游戏中奖号码的生成，中奖号码由 6 个红球号码和 1 个蓝球号码组成。
 * 其中红球号码要求随机生成 6 个 1~33 之间不重复的随机号码。
 * 其中蓝球号码要求随机生成 1 个 1~16 之间的随机号码。
 *
 * @author chaobo
 * @date 2021/1/7 12:19
 */
public class Question1_3 {

    public static void main(String[] args) {
        Random ran = new Random();
        int[] redBalls = new int[6];
        for (int i = 0; i < 6; i++) {
            while (0 == redBalls[i]) {
                redBalls[i] = ran.nextInt(33);
            }
            if (0 == i) {
                continue;
            }
            int j = 0;
            while (j < i) {
                if (0 == redBalls[i] || redBalls[i] == redBalls[j]) {
                    redBalls[i] = ran.nextInt(33);
                } else {
                    j++;
                }
            }
        }

        int blueBall = 0;
        while (0 == blueBall) {
            blueBall = ran.nextInt(16);
        }

        int[] doubleCollerBall = new int[7];
        System.arraycopy(redBalls, 0, doubleCollerBall, 0, redBalls.length);
        doubleCollerBall[6] = blueBall;

        System.out.print("双色球的中奖号码是：");
        System.out.println(Arrays.toString(doubleCollerBall));
    }
}
