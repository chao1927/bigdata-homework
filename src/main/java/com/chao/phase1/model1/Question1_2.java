package com.chao.phase1.model1;

/**
 * 2. 编程题
 * 编程找出 1000 以内的所有完数并打印出来。
 * 所谓完数就是一个数恰好等于它的因子之和， 如： 6=1＋2＋3
 *
 * @author chaobo
 * @date 2021/1/7 12:19
 */
public class Question1_2 {

    public static void main(String[] args) {
        //循环判断输出1~1000之间所有的完数
        System.out.println("以下为1000以内的完数：");
        int count = 0;
        for (int i = 2; i <= 1000; i++) {
            if (checkIs(i)) {
                count ++;
            }
        }
        System.out.println();
        System.out.println("1000以内的完数一共有" + count + "个。");
    }

    //判断是否为完数
    public static boolean checkIs(int i) {
        int sum = 0;
        //用1~i-1去整除i，如果能被整除，则j即为i的因子，加到sum中去。
        for (int j = 1; j < i; j++) {
            if (i % j == 0) {
                sum += j;
            }
        }
        //如果i所有因子相加后的sum和i本身的值相同，则i为完数，并计数加一。
        if (sum == i) {
            System.out.print(i + "\t");
            return true;
        }
        return false;
    }
}
