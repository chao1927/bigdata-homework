package com.chao.phase1.model2;

import java.util.Random;

/**
 * @author chaobo
 * @date 2021/2/23 22:26
 */
public class Question2_1 {

    private int row;

    private int column;

    private int[][] arrs;

    private int[] sumRows;

    private int[] sumCols;

    private int[] sumDiagonal;


    public Question2_1() {
    }

    public Question2_1(int row, int column) {
        this.row = row;
        this.column = column;
        setSumDiagonal(2);
        sumRows = new int[row];
        sumCols = new int[column];
        arrs = new int[row][column];
    }

    public static void main(String[] args) {
        Question2_1 q1 = new Question2_1(16, 16);

        //添加元素
        q1.add();
        q1.show();

        //行的和
        q1.getSumRows();

        //列的和
        q1.getSumCols();

        //对角线的和
        q1.getSumDiagonal();
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int[][] getArrs() {
        return arrs;
    }

    public void setArrs(int[][] arrs) {
        this.arrs = arrs;
    }

    public int[] getSumRows() {
        System.out.println("\n====打印各行的和====");
        for (int i = 0; i < sumRows.length; i++) {
            System.out.println("第" + i + "列的和" + sumRows[i]);
        }
        return sumRows;
    }

    public void setSumRows(int[] sumRows) {
        this.sumRows = sumRows;
    }

    /**
     * 获得全部列的和
     *
     * @return
     */
    public int[] getSumCols() {
        System.out.println("\n====打印各列的和====");
        for (int i = 0; i < sumCols.length; i++) {
            System.out.println("第" + i + "行的和" + sumCols[i]);
        }
        return sumCols;
    }

    public void setSumCols(int[] sumCols) {
        this.sumCols = sumCols;
    }

    public int[] getSumDiagonal() {
        System.out.println("\n====对角线的和====");
        System.out.println("从左上角到右下角的和" + sumDiagonal[0]);
        System.out.println("从右下角到左上角的和" + sumDiagonal[1]);
        return sumDiagonal;
    }

    public void setSumDiagonal(int number) {
        sumDiagonal = new int[number];
    }

    public void add() {
        Random ra = new Random();
        for (int i = 0; i < arrs.length; i++) {
            for (int j = 0; j < arrs[i].length; j++) {
                arrs[i][j] = ra.nextInt(10);
                sumRows[i] += arrs[i][j];
                sumCols[j] += arrs[i][j];
                if (i == j) {
                    sumDiagonal[0] += arrs[i][j];
                }
                if (i == (arrs[i].length - 1 - j)) {
                    sumDiagonal[1] += arrs[i][j];
                }
            }
        }
    }

    /**
     * 打印数组
     */

    public void show() {
        for (int i = 0; i < arrs.length; i++) {
            for (int j = 0; j < arrs[i].length; j++) {
                System.out.print(" " + arrs[i][j]);
            }
            System.out.println();
        }
    }
}
