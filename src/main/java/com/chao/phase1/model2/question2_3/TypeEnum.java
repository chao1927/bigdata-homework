package com.chao.phase1.model2.question2_3;

/**
 * @author chaobo
 * @date 2021/2/23 22:42
 */
public enum TypeEnum {

    /**
     * 大卡
     */
    BIG("大卡"),

    /**
     * 小卡
     */
    SMALL("小卡"),

    /**
     * 微型卡
     */
    MINI("微型卡");

    private final String cardType;

    private TypeEnum(String cardType) {
        this.cardType = cardType;
    }

    public String getCardType() {
        return cardType;
    }

}
