package com.chao.phase1.model2.question2_3;

/**
 * @author chaobo
 * @date 2021/2/23 22:38
 */
public class UserConsume {

    private int consumeValue;
    private int consumeCallDuration;
    private int consumeNetworkData;

    public int getConsumeValue() {
        return consumeValue;
    }

    public void setConsumeValue(int consumeValue) {
        this.consumeValue = consumeValue;
    }

    public int getConsumeCallDuration() {
        return consumeCallDuration;
    }

    public void setConsumeCallDuration(int consumeCallDuration) {
        this.consumeCallDuration = consumeCallDuration;
    }

    public int getConsumeNetworkData() {
        return consumeNetworkData;
    }

    public void setConsumeNetworkData(int consumeNetworkData) {
        this.consumeNetworkData = consumeNetworkData;
    }

    /**
     * 打印消费状况
     */
    public void showInfo() {
        System.out.print("\n消费状况：您已通话 " + consumeCallDuration + "分钟；流量使用" + consumeNetworkData + "GB;合计消费" + consumeValue + "元。");
    }

}
