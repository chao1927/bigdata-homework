package com.chao.phase1.model2.question2_3;

/**
 * @author chaobo
 * @date 2021/2/23 22:39
 */
public class PhoneCard {

    private String cardType;
    private String cardId;
    private String userName;
    private String password;
    private int accountBalance;
    private int callDuration;
    private int networkData;
    private UserConsume userConsume;

    public PhoneCard() {
    }

    public PhoneCard(String cardType, String cardId, String userName, String password, int accountBalance, int callDuration, int networkData, UserConsume userConsume) {
        this.cardType = cardType;
        this.cardId = cardId;
        this.userName = userName;
        this.password = password;
        this.accountBalance = accountBalance;
        this.callDuration = callDuration;
        this.networkData = networkData;
        this.userConsume = userConsume;
    }

    public UserConsume getUserConsume() {
        return userConsume;
    }

    public void setUserConsume(UserConsume userConsume) {
        this.userConsume = userConsume;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(int accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getCallDuration() {
        return callDuration;
    }

    public void setCallDuration(int callDuration) {
        this.callDuration = callDuration;
    }

    public int getNetworkData() {
        return networkData;
    }

    public void setNetworkData(int networkData) {
        this.networkData = networkData;
    }

    /**
     * 增长消费方法
     *
     * @param consumeValue        消费金额
     * @param consumeCallDuration 通话时长
     * @param consumeNetworkData  上网流量
     */
    public void addComsume(int consumeValue, int consumeCallDuration, int consumeNetworkData) {
        // 记录消费金额
        getUserConsume().setConsumeValue(getUserConsume().getConsumeValue() + consumeValue);
        // 记录通话时长
        getUserConsume().setConsumeCallDuration(getUserConsume().getConsumeCallDuration() + consumeCallDuration);
        //记录上网流量
        getUserConsume().setConsumeNetworkData(getUserConsume().getConsumeNetworkData() + consumeNetworkData);
    }

    //打印信息
    public void showInfo() {
        System.out.printf("卡号：%s\n用户名：%s\n当前余额：%d元\n", getCardId(), getUserName(), getAccountBalance());
        // 打印消费状况
        getUserConsume().showInfo();
    }

    /**
     * 多态
     * 打印用户全部信息与各种套餐内容
     *
     * @param abstractPackage 套餐抽象类对象
     */
    public void showOnCreate(AbstractPackage... abstractPackage) {
        System.out.printf("卡号：%s\n类型：%s\n用户名：%s\n密码：%s\n时长包：%d分钟\n流量包：%dGB\n当前余额：%d元\n", getCardId(), getCardType(),
                getUserName(), getPassword(), getCallDuration(), getNetworkData(), getAccountBalance());
        for (int i = 0; i < abstractPackage.length; i++) {
            abstractPackage[i].showInfo();
        }
    }

}
