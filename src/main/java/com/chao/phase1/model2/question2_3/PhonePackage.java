package com.chao.phase1.model2.question2_3;

/**
 * @author chaobo
 * @date 2021/2/23 22:40
 */
public class PhonePackage extends AbstractPackage implements ServeCall {

    private int message;

    /**
     * 初始化通话套餐
     */
    public PhonePackage(int price, int phonePackage, int message) {
        super(price, phonePackage);
        this.message = message;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(final int message) {
        this.message = message;
    }

    @Override
    public void showInfo() {
        System.out.print("通话套餐：时长包" + getPhonePackage() + "分钟;每个月资费" + getPrice() + "元/分钟;短信" + getMessage() + "条");
    }

    @Override
    public void server(int number, PhoneCard phoneCard) {
        // 消费金额=通话数量*资费
        int consumeValue = number * getPrice();
        // 手机余额减消费
        phoneCard.setAccountBalance(phoneCard.getAccountBalance() - consumeValue);
        // 添加通话消费时长
        phoneCard.addComsume(consumeValue, number, 0);
        // 打印手机卡当前消费信息
        phoneCard.showInfo();
    }


}
