package com.chao.phase1.model2.question2_3;

/**
 * @author chaobo
 * @date 2021/2/23 22:39
 */
public abstract class AbstractPackage {

    /**
     * 套餐资费
     */
    private int price;

    /**
     * 套餐数量
     */
    private int phonePackage;

    /**
     * 无参构造
     */
    public AbstractPackage() {
    }

    /**
     * 初始化套餐
     */
    public AbstractPackage(int price, int phonePackage) {
        setPrice(price);
        setPhonePackage(phonePackage);
    }

    /**
     * 设置资费
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * 获取资费
     */
    public int getPrice() {
        return price;
    }

    /**
     * 设置套餐数量
     */
    public void setPhonePackage(int phonePackage) {
        this.phonePackage = phonePackage;
    }

    /**
     * 获取套餐
     */
    public int getPhonePackage() {
        return phonePackage;
    }

    /**
     * 打印套餐信息
     */
    public abstract void showInfo();

}
