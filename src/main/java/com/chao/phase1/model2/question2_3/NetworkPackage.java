package com.chao.phase1.model2.question2_3;

/**
 * @author chaobo
 * @date 2021/2/23 22:41
 */
public class NetworkPackage extends AbstractPackage implements ServerNtework {

    public NetworkPackage(int price, int phonePackage) {
        super(price, phonePackage);
    }

    @Override
    public void showInfo() {
        System.out.print("上网套餐：流量包" + getPhonePackage() + "GB;每个月资费" + getPrice() + "元/GB");
    }

    @Override
    public void server(int networkData, PhoneCard phoneCard) {
        // 消费金额=上网流量*资费
        int consumeValue = networkData * getPrice();
        // 手机余额减消费
        phoneCard.setAccountBalance(phoneCard.getAccountBalance() - consumeValue);
        // 添加流量消费数量
        phoneCard.addComsume(consumeValue, 0, networkData);
        // 打印手机卡当前消费信息
        phoneCard.showInfo();
    }

}
