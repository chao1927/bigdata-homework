package com.chao.phase1.model2.question2_3;

/**
 * @author chaobo
 * @date 2021/2/23 22:40
 */
public interface ServeCall {

    /**
     * 通话数量，手机卡对象
     */
    void server(int number, PhoneCard phoneCard);
}
