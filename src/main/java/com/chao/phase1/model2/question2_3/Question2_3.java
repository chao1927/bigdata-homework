package com.chao.phase1.model2.question2_3;

/**
 * @author chaobo
 * @date 2021/2/23 22:42
 */
public class Question2_3 {

    public static void main(String[] args) {
        System.out.println("========注册卡=========");
        // 定义通话套餐数量与资费
        PhonePackage call = new PhonePackage(1, 0, 0);
        // 定义上网套餐与资费
        NetworkPackage network = new NetworkPackage(1, 0);
        // 消费统计对象
        UserConsume consume = new UserConsume();
        // 注册一张手机卡  类型：大卡
        PhoneCard card = new PhoneCard(TypeEnum.BIG.getCardType(), "110120130150",
                "username", "******", 1000, call.getPhonePackage(), network.getPhonePackage(), consume);
        // 多态：打印套餐信息：抽象套餐类->通话套餐、上网套餐
        card.showOnCreate(call, network);
        System.out.println("=================\n\n");
        // 开始消费
        System.out.println("========使用通话=========");
        call.server(30, card);
        System.out.println("\n=================\n\n");

        System.out.println("========使用流量=========");
        network.server(100, card);
        System.out.println("\n=================\n\n");
    }

}
